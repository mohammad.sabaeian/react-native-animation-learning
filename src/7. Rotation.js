import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Rotation extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        this.state.animation.setValue(0);
        Animated.timing(this.state.animation, {
            toValue:360,
            duration:5000
        }).start(this.start);
    }

    render() {
        const rotateInterpolate = this.state.animation.interpolate({
            inputRange:[0 , 360],
            outputRange:['0deg','360deg']
        })
        const aStyles = {
            transform:[
                {
                    rotate:rotateInterpolate
                }
            ]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});