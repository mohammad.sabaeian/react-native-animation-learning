import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback , Dimensions } from 'react-native'

export default class Corners extends Component {

    state = {
        animation: new Animated.ValueXY()
    }

    start = () => {
        const {width , height} = Dimensions.get('window');

        Animated.sequence([
             Animated.spring(this.state.animation.y,{
                 toValue:height - this._height,
                 speed:1,
                 useNativeDriver:true
             }),
             Animated.spring(this.state.animation.x,{
                 toValue:width - this._width,
                 speed:1,
                 useNativeDriver:true
             }),
             Animated.spring(this.state.animation.y,{
                 toValue:0,
                 speed:1,
                 useNativeDriver:true
             }),
             Animated.spring(this.state.animation.x,{
                 toValue:0,
                 speed:1,
                 useNativeDriver:true
             }),
        ]).start();
    }

    saveDimensions = (e) => {
        this._width = e.nativeEvent.layout.width;
        this._height = e.nativeEvent.layout.height;
    }

    render() {
        const aStyles = {
            transform:this.state.animation.getTranslateTransform()
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start} onLayout={this.saveDimensions}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red',
       position:'absolute',
       left:0,
       top:0
   }
});