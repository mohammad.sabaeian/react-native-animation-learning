import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class WidthHeight extends Component {

    state = {
        animation: new Animated.Value(150)
    }

    start = () => {
        Animated.timing(this.state.animation, {
            toValue:300,
            duration:800,
        }).start();
    }

    render() {
        const aStyles = {
            width:this.state.animation,
            height:this.state.animation
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
    //    width:200,
    //    height:200,
       backgroundColor:'red'
   }
});