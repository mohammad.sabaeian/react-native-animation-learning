import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Interpolation extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.sequence([
            Animated.timing(this.state.animation , {
                toValue:1,
                duration:2000
            }),
            Animated.delay(1200),
            Animated.timing(this.state.animation,{
                toValue:2,
                duration:1000
            })
        ]).start()
    }

    render() {
        const animatedInterpolated = this.state.animation.interpolate({
            inputRange:[0 , 1 , 2],
            outputRange:[0 , 300 ,0]
        });

        const interpolatedInterpolate = animatedInterpolated.interpolate({
            inputRange:[0 , 300],
            outputRange: [ 1 , 0.5 ]
        });

        const translateXInerpolate = animatedInterpolated.interpolate({
            inputRange:[0. , 30 , 50 , 80 , 100 , 280],
            outputRange:[0 , -30 , -50 , 20 , 90 , 300],
            extrapolate:'clamp'
        })

        const coloInterpolate = this.state.animation.interpolate({
            inputRange: [ 0 , 1 , 2],
            outputRange:['rgba(0,0,0,1)','rgba(190,130,152,0.5)','rgba(100,100,100,0.2)']
        })

        const aStyles = {
            transform:[
                {
                    translateY: animatedInterpolated
                },
                {
                    translateX:translateXInerpolate
                }
            ],
            opacity:interpolatedInterpolate,
            backgroundColor:coloInterpolate
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});