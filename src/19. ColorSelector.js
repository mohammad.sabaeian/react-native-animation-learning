import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableOpacity , TouchableWithoutFeedback , TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/Foundation'

const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
const AnimatedIcon = Animated.createAnimatedComponent(Icon);


export default class ColorSelector extends Component {

    state = {
        animation: new Animated.Value(0),
        buttonAnimation: new Animated.Value(0),
        color:'#000',
        inputOpen:false
    }

    handleToggle = () => {
        const toValue = this._open ? 0 : 1;
        Animated.spring(this.state.animation , {
            toValue
        }).start();
        this._open = !this._open;
    }

    toggleInput = () => {
        const toValue = this._inputOpen ? 0 : 1;
        Animated.timing(this.state.buttonAnimation , {
            toValue,
            duration:350
        }).start();

        this._inputOpen = !this._inputOpen;
        this.setState({
            inputOpen:this._inputOpen
        })
    }

    render() {

        const scaleXInterpolate = this.state.animation.interpolate({
            inputRange:[0 , 0.5 , 1],
            outputRange:[0 , 0 , 1]
        });

        const translateYInterpolate = this.state.animation.interpolate({
            inputRange:[0 , 1],
            outputRange:[150 , 0]
        });
        
        const moveInterpolate = this.state.buttonAnimation.interpolate({
            inputRange:[0 , 1],
            outputRange:[-155 , 0]
        });

        const buttonScaleInterpolate = this.state.buttonAnimation.interpolate({
            inputRange:[0 , 1],
            outputRange:[0.01 , 1]
        });

        const buttonStyle = {
            transform:[
                {
                    translateX:moveInterpolate
                },
                {
                    scale:buttonScaleInterpolate
                }
            ]
        }

        const rowStyle = {
            opacity:this.state.animation,
            transform:[
                {
                    scaleY:this.state.animation
                },
                {
                    scaleX:scaleXInterpolate
                },
                {
                    translateY:translateYInterpolate
                }
            ]
        }

        const inputOpacityInterpolate = this.state.buttonAnimation.interpolate({
            inputRange:[0 , 0.8 , 1],
            outputRange:[0 , 0 ,1]
        });

        const IconTranslate = this.state.buttonAnimation.interpolate({
            inputRange:[0 , 1],
            outputRange:[0 , -20]
        });

        const opacityIconInterpolate = this.state.buttonAnimation.interpolate({
            inputRange:[0 , .2],
            outputRange:[1 , 0],
            extrapolate:'clamp'
        });

        const iconStyle = {
            opacity:opacityIconInterpolate,
            transform:[
                {
                    translateX:IconTranslate
                }
            ]
        }

        const inputStyle = {
            opacity:inputOpacityInterpolate
        }
        const colorStyle = {
            backgroundColor:this.state.color
        }
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.rowWrap,rowStyle]}>
                    <TouchableWithoutFeedback onPress={this.toggleInput}>
                        <Animated.View style={[styles.colorBall,colorStyle]} />
                    </TouchableWithoutFeedback>
                    <View style={styles.row}>
                        <TouchableOpacity>
                            <AnimatedIcon name="bold" size={30} color='#555' style={iconStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <AnimatedIcon name="italic" size={30} color='#555' style={iconStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <AnimatedIcon name="align-center" size={30} color='#555' style={iconStyle} />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <AnimatedIcon name="link" size={30} color='#555' style={iconStyle} />
                        </TouchableOpacity>
                        <Animated.View
                            style={[StyleSheet.absoluteFill , styles.colorRowWrap]}
                            pointerEvents={this.state.inputOpen ? 'auto' : 'none'}
                        >
                            <AnimatedTextInput 
                                style={[styles.input,inputStyle]}
                                value={this.state.color}
                                onChangeText={color => this.setState({color})}
                                ref={input => this._input = input}
                            />
                            <TouchableWithoutFeedback>
                                <Animated.View style={[styles.okayButton  , buttonStyle]}>
                                    <Text style={[styles.okayText]}>OK</Text>
                                </Animated.View>
                            </TouchableWithoutFeedback>
                        </Animated.View>
                    </View>
                </Animated.View>
                <TouchableOpacity onPress={this.handleToggle} style={{margin:10}}>
                    <Text>Toggle Open/Closed</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ffc',
        justifyContent:'center',
        alignItems:'center'
    },
    rowWrap:{
        flexDirection:'row',
        alignItems:'center',
        minWidth:'40%',
        backgroundColor:'white',
        borderRadius:20,
        elevation:1,
        paddingVertical:15,
        paddingHorizontal:10
    },
    colorBall:{
        width:25,
        height:25,
        borderRadius:12
    },
    input:{
        flex:1
    },
    okayButton:{
        borderRadius:20,
        width:50,
        height:'100%',
        backgroundColor:'#309EEb',
        alignItems:'center',
        justifyContent:'center'
    },
    okayText:{
        color:'white'
    },
    row:{
        flex:1,
        alignItems:'center',
        justifyContent:'space-around',
        flexDirection:'row',
        overflow:'hidden'
    },
    colorRowWrap:{
        flexDirection:'row',
        flex:1,
        paddingLeft:5
    }
});