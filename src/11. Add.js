import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Add extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        
    }

    render() {
        
        // const newAnimation = Animated.add(this.state.animation, 50)
        // const newAnimation = Animated.divide(this.state.animation, 5)
        // const newAnimation = Animated.multiply(this.state.animation, 2)
        const newAnimation = Animated.modulo(this.state.animation , 3);
        const interpolated = newAnimation.interpolate({
            inputRange:[0 , 3],
            outputRange:['0deg','270deg']
        })
        const aStyles = {
            // transform: [{translateY: newAnimation}]
            transform: [{rotate: interpolated}]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});