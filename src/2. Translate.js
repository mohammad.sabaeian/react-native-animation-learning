import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Translate extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.timing(this.state.animation , {
            toValue:400,
            duration:2000
        }).start(()=> {
            this.state.animation.setValue(0)
        })
    }

    render() {
        const animationStyles = {
            transform : [
                {
                    translateY: this.state.animation
                }
            ]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,animationStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});