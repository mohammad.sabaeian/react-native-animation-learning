import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Color extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.timing(this.state.animation , {
            toValue:1,
            duration:1250
        }).start(() => {
            Animated.timing(this.state.animation , {
                toValue:0,
                duration:6000
            }).start()
        })
    }

    render() {
        const boxInterpolation = this.state.animation.interpolate({
            inputRange: [ 0 , 1 ],
            outputRange:['rgb(255,99,71)','rgb(99,71,255)']
        });
        boxAnimationStyle = {
            backgroundColor:boxInterpolation
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,boxAnimationStyle]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});