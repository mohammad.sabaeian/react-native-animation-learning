import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback , PanResponder } from 'react-native'
import { tryStatement } from '@babel/types';

export default class Decay extends Component {

    state = {
        animation: new Animated.ValueXY(0)
    }

    componentWillMount(){
        this._x = 0;
        this._y = 0;

        this.state.animation.addListener(({ x , y}) => {
            this._x = x;
            this._y = y;
        }) 
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponder:() => true,
            onPanResponderGrant:() => {
                this.state.animation.setOffset({
                    x:this._x,
                    y:this._y
                });
                this.state.animation.setValue({
                    x:0,
                    y:0
                })
            },
            onPanResponderMove: Animated.event([
                null , 
                {
                    dx:this.state.animation.x,
                    dy:this.state.animation.y
                }
            ]),
            onPanResponderRelease: (e , { vx , vy }) => {
                Animated.decay(this.state.animation , {
                    velocity: { x : vx , y:vy},
                    deceleration: 0.997
                }).start();
            }
        });
    }


    start = () => {

    }

    render() {
        const aStyles = {
            transform:this.state.animation.getTranslateTransform()
        }
        return (
            <View style={styles.container}>
                {/* <TouchableWithoutFeedback onPress={this.start}> */}
                    <Animated.View style={[styles.box,aStyles]} { ...this._panResponder.panHandlers } />
                {/* </TouchableWithoutFeedback> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});