import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , PanResponder , Dimensions } from 'react-native'

import Php from './img/php.png'
import Js from './img/js.png'
import Java from './img/java.png'

const SWIPE_THRESHOLD = 120;
const { height , width } = Dimensions.get("window");
import clamp from 'clamp'

export default class Cards extends Component {

    state = {
        items: [
            {
                image: Php,
                id: 1,
                text: "Php sucks",
            },
            {
                image: Js,
                id: 2,
                text: "Javascipt is great",
            },
            {
                image: Java,
                id: 3,
                text: "java?! come on man",
            }
        ],
            animation: new Animated.ValueXY(),
            opacity: new Animated.Value(1),
            next: new Animated.Value(0.9),
        
    }

    componentWillMount(){
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponder:() => true , 
            onPanResponderMove: Animated.event([
                null ,
                {
                    dx:this.state.animation.x,
                    dy:this.state.animation.y
                }
            ]),
            onPanResponderRelease:(e , {dx , vx , vy}) => {
                let velocity;
                
                if(vx >= 0){
                    velocity = clamp(vx , 3 , 5);
                }else if(vx < 0){
                    velocity = clamp(Math.abs(vx) , 3 , 5) * -1;
                }

                if(Math.abs(dx) > SWIPE_THRESHOLD){
                    Animated.decay(this.state.animation, {
                        velocity:{x:velocity , y:vy},
                        deceleration:0.98
                    }).start(this.transitionNext);
                }else{
                    Animated.spring(this.state.animation , {
                        toValue:{x:0,y:0},
                        friction:4
                    }).start();
                }
            }
        })
    }

    transitionNext = () => {
        Animated.parallel([
            Animated.timing(this.state.opacity , {
                toValue:0,
                duration:300
            }),
            Animated.spring(this.state.next,{
                toValue:1,
                friction:4
            })
        ]).start(() => {
            this.setState((state) => {
                return {
                    items:state.items.slice(1)
                }
            }, () => {
                this.state.next.setValue(0.9);
                this.state.opacity.setValue(1);
                this.state.animation.setValue({x:0, y:0});
            });
        })
        
    }

    render() {
        const {animation} = this.state;
        const rotate = animation.x.interpolate({
            inputRange:[-width , 0 , width],
            outputRange:['-40deg' , '0deg' , '40deg'],
            extrapolate:'clamp'
        });

        const opacity = animation.x.interpolate({
            inputRange: [-width, 0 , width],
            outputRange:[0.5 , 1 , 0.5],
            extrapolate:'clamp'
        });

        const animatedCardStyles = {
            opacity:this.state.opacity,
            transform:[
                {
                    rotate
                },
                ... this.state.animation.getTranslateTransform(),

            ]
        }
        
        const animatedImageStyles = {
            opacity
        }

        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    {
                        this.state.items.slice(0 , 2).reverse().map(({image , id , text} , index , items) => {
                            const isLastItem = index === items.length-1
                            const isSecondToLast = index === items.length-2

                            const panHandlers = isLastItem? this._panResponder.panHandlers : {};
                            const cardStyle = isLastItem ? animatedCardStyles : undefined;
                            const imageStyle = isLastItem ? animatedImageStyles : undefined;

                            const nextStyle = isSecondToLast ? {
                                transform:[{scale:this.state.next}]
                            } : undefined;

                            return (
                                <Animated.View key={id} style={[styles.card,cardStyle,nextStyle]} {...panHandlers}>
                                    <Animated.Image 
                                        source={image}
                                        resizeMode="center"
                                        style={[styles.image,imageStyle]}
                                    />
                                    <View style={styles.lowerText}>
                                        <Text>{text}</Text>
                                    </View>
                                </Animated.View>
                            )
                        })
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ffc',
        justifyContent:'center',
        alignItems:'center'
    },
    top: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    },
    buttonBar:{
        flexDirection:'row',
        justifyContent:'center',
        alignContent:'center',
        paddingVertical:20
    },
    card: {
        width: 400,
        height: 400,
        position: "absolute",
        borderRadius: 3,
        shadowColor: "#000",
        shadowOpacity: 0.1,
        shadowOffset: { x: 0, y: 0 },
        shadowRadius: 5,
        borderWidth: 1,
        borderColor: "#FFF",
        backgroundColor:'white',
        elevation:1
    },
    lowerText: {
        flex: 1,
        backgroundColor: "#FFF",
        padding: 5,
        textAlign:'center'
    },
    image: {
        width: null,
        height: null,
        borderRadius: 2,
        flex: 2,
    },
});