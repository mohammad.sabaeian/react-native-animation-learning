import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Opacity extends Component {

    state = {
        animation: new Animated.Value(1)
    }

    start = () => {
        Animated.timing(this.state.animation, {
            toValue:0,
            duration:400,
        }).start(() => {
            Animated.timing(this.state.animation , {
                toValue:1,
                duration:800
            }).start()
        });
    }

    render() {
        const animationStyle = {
            opacity:this.state.animation
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,animationStyle]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});