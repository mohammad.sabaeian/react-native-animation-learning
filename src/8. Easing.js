import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , Easing , TouchableWithoutFeedback } from 'react-native'

export default class EasingComponent extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.timing(this.state.animation, {
            toValue:500,
            duration:1000,
            // easing:Easing.back(5),
            // easing:Easing.bounce,
        }).start()
    }

    render() {
        const aStyles = {
            transform:[
                {
                    translateY:this.state.animation
                }
            ]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});