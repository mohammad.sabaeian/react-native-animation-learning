import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , Button } from 'react-native'


const AnimatedButton = Animated.createAnimatedComponent(Button);

export default class CreateAnimatedComponent extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.sequence([
            Animated.timing(this.state.animation , {
                toValue:1,
                duration:2000
            }),
            Animated.delay(1200),
            Animated.timing(this.state.animation,{
                toValue:2,
                duration:1000
            })
        ]).start()
    }

    render() {
        const color = this.state.animation.interpolate({
            inputRange:[0 , 1 , 2],
            outputRange:['green','blue','red']
        })
        return (
            <View style={styles.container}>
                <AnimatedButton onPress={this.start} color={color} title="کلیک کن" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});