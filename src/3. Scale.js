import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Scale extends Component {

    state = {
        animation: new Animated.Value(1)
    }

    start = () => {
        Animated.timing(this.state.animation , {
            toValue:4,
            duration:600
        }).start(()=> {
            Animated.timing(this.state.animation , {
                toValue:0.5,
                duration:300
            }).start()
        })
    }

    render() {
        const aStyles = {
            transform:[
                {
                    scale:this.state.animation
                }
            ]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});