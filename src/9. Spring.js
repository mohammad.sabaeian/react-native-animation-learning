import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Spring extends Component {

    state = {
        animation: new Animated.Value(1)
    }

    start = () => {
        this.state.animation.addListener(({value}) => console.log(value))
        Animated.spring(this.state.animation , {
            toValue:2,
            friction:2,
            tension:160
        }).start(() => {
            Animated.timing(this.state.animation , {
                toValue:1,
                duration:100
            })
        });
    }

    render() {
        const aStyles = {
            transform : [
                {scale:this.state.animation}
            ]
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       width:200,
       height:200,
       backgroundColor:'red'
   }
});