import React, { Component } from 'react'
import { Text, View , StyleSheet , Animated , TouchableWithoutFeedback } from 'react-native'

export default class Position extends Component {

    state = {
        animation: new Animated.Value(0)
    }

    start = () => {
        Animated.timing(this.state.animation , {
            toValue:300,
            duration:1000
        }).start();
    }

    render() {
        const aStyles = {
            top:this.state.animation,
            left:this.state.animation
        }
        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={this.start}>
                    <Animated.View style={[styles.box,aStyles]} />
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:'#ffc',
       justifyContent:'center',
       alignItems:'center'
   },
   box:{
       position:'absolute',
       width:200,
       height:200,
       backgroundColor:'red'
   }
});