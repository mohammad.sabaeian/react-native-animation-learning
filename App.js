/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import Opacity from './src/1- Opacity';
import Translate from './src/2. Translate';
import Scale from './src/3. Scale';
import WidthHeight from './src/4. WidthHeight';
import Position from './src/5. Position';
import Color from './src/6. Color';
import Rotation from './src/7. Rotation';
import Easing from './src/8. Easing';
import Spring from './src/9. Spring';
import Decay from './src/10. Decay';
import Add from './src/11. Add';
import Interpolation from './src/13. Interpolation';
import CreateAnimatedComponent from './src/14. CreateAnimatedComponent';
import Flubber from './src/15. Flubber';
import Corners from './src/16. Corners';
import Cards from './src/17. Cards';
import Login from './src/18.Login';
import ColorSelector from './src/19. ColorSelector';

export default ColorSelector;